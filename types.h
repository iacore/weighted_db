#pragma once
#include <stddef.h>
#include <string.h>

typedef float f32;
typedef unsigned char u8;
typedef size_t usize;

typedef struct Slice Slice;
struct [[gnu::packed]] Slice {
    [[gnu::aligned(alignof(void*))]]
    char* ptr;
    usize len;
};

typedef struct Entry Entry;
struct [[gnu::packed]] Entry {
    [[gnu::aligned(alignof(void*))]] // to quell warning
    Entry* prev;
    Slice  key;
    Slice  value;
    f32    weight;
};

typedef struct Header Header;
struct [[gnu::packed]] Header {
    usize  version;
    [[gnu::aligned(alignof(void*))]] // to quell warning
    Entry* last;
    usize  sp;
};
