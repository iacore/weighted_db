
weighted linked list database, idea inspired by [a simple flashcard program](https://git.exozy.me/a/SDC/), memory layout inspired by [RetroForth](https://retroforth.org/).

uses C23 with GNU C extensions

data integrity is preserved as long as your OS kernel doesn't crash

compaction can be done with a moving GC (not implemented)

## footguned

I hate programming in C.

- `open(_, O_CREATE)` takes `mode` (random bits) from the stack. subsequent `open` fails only sometimes depending on the actual file mode.
- passing `va_list` to a vararg function gives garbage
- `unreachable()` not exist in Clang despite in C23
- `__builtin_unreachable()` compiles to literally nothing.
- `int err;` initialize to 0 or garbage depending on how much stack space the surrounding function need. when `ctf_*` produce no error, the content of `&err` is not modified.
