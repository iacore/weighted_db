#define _POSIX_C_SOURCE 200112L

#include <stddef.h>

typedef float f32;
typedef unsigned char u8;
typedef size_t usize;

const usize we_map_at = 0x400000000000;
const usize db_size = 0x100000;

#include "types.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

[[noreturn]]
void unreachable() {
    puts("reached hypothetically unreachable code");
    abort();
}

[[noreturn]]
void unimplemented() {
    puts("unimplemented");
    abort();
}

Slice span(char* s) {
    return (Slice) { .ptr = s, .len = strlen(s) };
}
bool seql(Slice lhs, Slice rhs) {
    return lhs.len == rhs.len && strncmp(lhs.ptr, rhs.ptr, lhs.len) == 0;
}

[[gnu::alloc_align(3)]]
void* hzalloc(Header* header, usize len, usize alignment) {
    usize offset = header->sp % alignment;
    if (offset != 0) offset = alignment - offset;
    if (header->sp + offset + len + sizeof(Header) >= db_size) {
        // buffer overflow here
        unimplemented();
    }
    void* ptr = memset((void*) header->sp + offset, 0, len);
    header->sp = header->sp + offset + len;
    return ptr;
}

Slice hstrcpy(Header* header, Slice s) {
    void* dst = hzalloc(header, s.len, 1);
    memcpy(dst, s.ptr, s.len);
    return (Slice) { .ptr = dst, .len = s.len };
}

void add_entry(Header* header, Slice key, Slice value, f32 weight) {
    Entry* mem = hzalloc(header, sizeof(Entry), alignof(Entry));
    mem->key = hstrcpy(header, key);
    mem->value = hstrcpy(header, value);
    mem->weight = weight;
    mem->prev = header->last;
    header->last = mem;
}

Entry* get_entry(Header* header, Slice key) {
    Entry* curr = header->last;
    while (curr != 0) {
        if(seql(curr->key, key)){
            break;
        }
        curr = curr->prev;
    }
    return curr;
}

void remove_one_entry(Header* header, Slice key) {
    Entry** curr_ptr = &header->last;
    while (*curr_ptr != 0) {
        if (seql((*curr_ptr)->key, key)) {
            memset((*curr_ptr)->key.ptr, 0, (*curr_ptr)->key.len);
            memset((*curr_ptr)->value.ptr, 0, (*curr_ptr)->value.len);
            memset((*curr_ptr), 0, sizeof(Entry));
            *curr_ptr = (*curr_ptr)->prev;
            break;
        }
        curr_ptr = &(*curr_ptr)->prev;
    }
}

// todo: compaction

typedef struct {
    f32 total_weight;
} Stat;

Stat calculate_stat(Header* header) {
    f32 total_weight = 0;
    Entry* curr = header->last;
    while (curr != 0) {
        total_weight += curr->weight;
        curr = curr->prev;
    }
    return (Stat) { .total_weight = total_weight };
}

// @return may be null
Entry* select_random_entry(Header* header, Stat stat) {
    f32 threshold = (f32)rand() * (f32)stat.total_weight / (f32)RAND_MAX;
    f32 accumulated = 0;
    Entry* curr = header->last;
    while (curr != 0) {
        accumulated += curr->weight;
        if (accumulated >= threshold) break;
        curr = curr->prev;
    }
    return curr;
}

#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>

void vprn (const char *__restrict __format, va_list args) {
    int err = vprintf(__format, args);
    if (err < 0) {
        unimplemented();
    }
    err = putchar('\n');
    if (err < 0) {
        unimplemented();
    }
}

[[gnu::format(printf, 1, 2)]]
void prn (const char *__restrict __format, ...) {
    va_list args;
    va_start(args, __format);
    vprn(__format, args);
    va_end(args);
}

[[noreturn]]
[[gnu::format(printf, 1, 2)]]
void fatalf (const char *__restrict __format, ...) {
    va_list args;
    va_start(args, __format);
    vprn(__format, args);
    va_end(args);
    _exit(1);
}

typedef enum { cmd_help, cmd_test, cmd_add, cmd_remove, cmd_pick, cmd_get } Command;
typedef struct {
    char*   program_name;
    Command cmd;
    bool    verbose;
    f32     add__weight;
    Slice   add__key;
    Slice   add__value;
    Slice   remove__key;
    Slice   get__key;
    bool    sincerely_want_help;
} ParsedCommand;

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdalign.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <ctf-api.h>

// use database layout in ELF .ctf and hash it
usize hash_db_layout(ParsedCommand task) {
    int err = 0;

    ctf_archive_t* archive = ctf_open(task.program_name, 0, &err);
    if (err != 0) {
        fatalf("%s", ctf_errmsg(err));
    }

    ctf_dict_t* dict = ctf_dict_open(archive, 0, &err);
    if (err != 0) {
        fatalf("%s", ctf_errmsg(err));
    }

    // get type id of the database header type 
    ctf_id_t type_Header = ctf_lookup_by_name(dict, "struct Header");
    err = ctf_errno(dict);
    if (err != 0) {
        fatalf("%s", ctf_errmsg(err));
    }
    if (task.verbose) {
        prn("typeid(struct Header)=%lu", type_Header);
    }

    // todo: hash the type from type_Header recursively

    ctf_dict_close(dict);
    ctf_close(archive);

    return 2; // placeholder. increment this number whenever the layout changes
}

int main (int argc, char** argv) {
    ParsedCommand task = {
        .program_name = argv[0],
        .cmd = cmd_help,
        .verbose = false,
        .sincerely_want_help = false
    };
    { // parseopt
        int i = 1;
        while (i < argc) {
            if (argv[i][0] == '-') {
                char opt = argv[i][1];
                switch (opt) {
                case 'v':
                    task.verbose = true;
                    break;
                case '-':
                    fatalf("we don't do --long-opt here, sorry");
                default:
                    fatalf("unknown option: %c", opt);
                }
                for (int j = i; j < argc - 1; ++j) {
                    argv[j] = argv[j+1];
                }
                --argc;
            } else {
                ++i;
            }
        }
    }
    if (argc >= 2) { // parsecmd
        if (strcmp(argv[1], "help") == 0) {
            if (argc == 2) {
                task.sincerely_want_help = true;
            }
        }
        if (strcmp(argv[1], "test") == 0) {
            if (argc == 2) {
                task.cmd = cmd_test;
            }
        }
        if (strcmp(argv[1], "add") == 0) {
            if (argc >= 4 && argc <= 5) {
                task.cmd = cmd_add;
                task.add__key = span(argv[2]);
                task.add__value = span(argv[3]);
                if (argc == 5) {
                    char* endptr;
                    task.add__weight = strtof(argv[4], &endptr);
                    if (endptr == argv[4]) {
                        fatalf("argument not a number: %s", argv[4]);
                    }
                } else {
                    task.add__weight = 1.0;
                }
            }
        }
        if (strcmp(argv[1], "get") == 0) {
            if (argc == 3) {
                task.cmd = cmd_get;
                task.get__key = span(argv[2]);
            }
        }
        if (strcmp(argv[1], "pick") == 0) {
            if (argc == 2) {
                task.cmd = cmd_pick;
            }
        }
        if (strcmp(argv[1], "remove") == 0) {
            if (argc == 3) {
                task.cmd = cmd_remove;
                task.remove__key = span(argv[2]);
            }
        }
    }
    if (task.cmd == cmd_help) {
        prn("Usage: %s [OPTIONS] COMMAND ...", task.program_name);
        prn("OPTIONS");
        prn("  -v  be more noisy");
        prn("COMMAND");
        prn("  add KEY VALUE [WEIGHT]");
        prn("    add key-value pair");
        prn("  get KEY");
        prn("    get value and weight by key");
        prn("  remove KEY");
        prn("    remove key-value pair by key");
        prn("  pick");
        prn("    pick at random");
        prn("  test");
        prn("    validate database version");
        prn("  help");
        prn("    print this message");
        return task.sincerely_want_help ? 0 : 1;
    }

    int fd = open("data", O_CREAT | O_RDWR, 0644);
    if (fd < 0) {
        fatalf("%s", strerror(errno));
    }
    // `fd` will be cleaned up on program exit

    int err = ftruncate(fd, db_size);
    if (err < 0) {
        fatalf("%s", strerror(errno));
    }

    void *db_mem = mmap((void*) we_map_at, db_size, PROT_READ | PROT_WRITE,
                        MAP_SHARED_VALIDATE | MAP_FIXED, fd, 0);
    if (db_mem == MAP_FAILED) {
        fatalf("%s", strerror(errno));
    }
    // `db_mem` will be cleaned up on program exit

    const usize db_layout = hash_db_layout(task);

    sigset_t sset;
    memset(&sset, 0xff, sizeof(sigset_t));
    err = sigprocmask(SIG_BLOCK, &sset, 0);
    if (err < 0) {
        fatalf("%s", strerror(errno));
    }

    Header* header = db_mem;
    if (header->version == 0) {
        header->version = db_layout;
        header->last = 0;
        header->sp = we_map_at + sizeof(Header);
        prn("initializing database with version %zu", db_layout);
    } else {
        if (header->version != db_layout) {
            fatalf("database version mismatch. expected: %zu. found: %zu.", db_layout, header->version);
        }
        prn("found valid database with version %zu", header->version);
    }

    switch (task.cmd) { // todo: log better
    case cmd_help:
        unreachable();
    case cmd_test:
        break;
    case cmd_add:
        add_entry(header, task.add__key, task.add__value, task.add__weight);
        break;
    case cmd_get: {
        Entry* entry = get_entry(header, task.get__key);
        if (entry == 0){
            prn("no entry");
        } else {
            prn("entry: %.*s -> %.*s (weight: %f)", (int)entry->key.len, entry->key.ptr, (int)entry->value.len, entry->value.ptr, entry->weight);
        }
        break;
    }
    case cmd_remove:
        remove_one_entry(header, task.remove__key);
        break;
    case cmd_pick: {
        Stat stat = calculate_stat(header);
        prn("total weight = %f", stat.total_weight);
        Entry* entry = select_random_entry(header, stat);
        if (entry == 0) {
            prn("no entry");
        } else {
            prn("entry: %.*s -> %.*s", (int)entry->key.len, entry->key.ptr, (int)entry->value.len, entry->value.ptr);
        }
        break;
    }
    }

    return 0;
}
