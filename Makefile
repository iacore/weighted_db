main: *.c *.h
	gcc -std=c2x -Wall -Wextra -g -gctf -lctf -o main main.c

clean:
	rm -f main
